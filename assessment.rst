Assessment guidelines
=====================

The following points will be used in assessing your implementation.

.. topic:: Code

   - Programs should execute without error.
   - Interfaces should conform to the specification in the
     Deliverables.
   - Correctness.
   - Clarity and structure of the implementations.

.. topic:: Documentation and process

   - Documentation of the library (both docstrings and comments in the
     code).
   - Unit tests.
   - Effective use of version control (commits of small steps with
     clear messages).
   - Balance of work within the team (as shown by the Git log).
