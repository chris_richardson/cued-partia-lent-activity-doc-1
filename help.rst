Getting help and feedback
=========================

Get started early to give your team time to seek feedback and resolve
issues. Issues/bugs are a feature of all software development and
engineering.


Help
----

Help channels for the activity are:

#. Peer support - this is encouraged but be sure that you understand
   what you are doing.

#. Discussion forum at
   https://www.allanswered.com/community/148/cued-part-ia-computing/. The
   forum is private to the IA computing course. If you resolve an
   issue in discussion with someone else, consider posting the
   question and resolution to the forum as a resource for others.

#. Timetabled support session in the DPO.


Feedback
--------

You can seek feedback on your work from demonstrators in the DPO
sessions. Bare in mind that there is a significant *design* component
to this project which means there is no one 'best' solution.

.. note::

   If there is sufficient interest, it may be possible to arrange
   personalised online support where you give a demonstrator access to
   you development repository.
