===========================================
Part IA Flood Monitoring and Warning System
===========================================

Lent Term 2017 (DRAFT)

Your team has been tasked with building the computational backend
(library) to a new real-time flood warning system for England and
Wales. The library should:

#. Fetch real-time river level data over the Internet from the
   Department for Environment Food and Rural Affairs data service.

#. Support specific data queries on river level monitoring stations.

#. Analyse monitoring station data in order to assess the flood risk,
   and issue flood warnings for areas of the country.

The required development practices are listed in the
:ref:`Requirements` section. The library is required to support
specific query interfaces (functions), as outlined in the
:ref:`Deliverables` section.  These are the *public interface* of the
library.  Another company has been contracted to build a user
interface using the public interfaces to the library.


.. rubric:: Development team

Your development team is your laboratory group.


.. rubric:: Document license and copyright

These documents are licensed under a Creative Commons
Attribution-ShareAlike 4.0 International License.  See
http://creativecommons.org/licenses/by-sa/4.0/ for the license.

Copyright by Garth N. Wells (gnw20@cam.ac.uk).


.. toctree::
   :maxdepth: 2
   :caption: Project specification

   requirements
   deliverables


.. toctree::
   :maxdepth: 1
   :caption: Appendix: Help and assessment

   getting-started
   development_recommendations
   help
   assessment
   objectives



.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
